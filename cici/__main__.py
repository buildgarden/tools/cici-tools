# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: MIT

from .main import main

if __name__ == "__main__":
    main()
